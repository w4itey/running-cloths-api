from weather import Weather
import pandas as pd


def zipcode(zipcode):

    df = pd.read_csv('static/Files/us-zip-code-latitude-and-longitude.csv', delimiter=';', index_col='Zip')

    return (df.at[zipcode, 'Latitude'], df.at[zipcode, 'Longitude'])


def Cloth(
        Latitude=None,
        Longitude=None,
        times=['07', '12', '18'],
        Gender=None,
        Zipcode=None):

    if Zipcode is not None:
        values = zipcode(Zipcode)
        Latitude = round(values[0], 5)
        Longitude = round(values[1], 5)
        print(Longitude)

    run = Weather()

    if times is None:
        times = ['07', '12', '18']

    results = run.getForecast(
        latitude=Latitude,
        longitude=Longitude,
        times=times
        )

    payload = {}
    iterate = len(times)

    for n in range(iterate):
        payload[n] = {
            'Temperature': results[n-1]['temperature'],
            'Forecast': results[n-1]['shortForecast'],
            'WindSpeed': results[n-1]['windSpeed'],
            'WindDirection': results[n-1]['windDirection'],
            'Time': times[n],
            'Gender': Gender
        }

    return payload


if __name__ == '__main__':

    # Cloth(33.820529, -84.370933, times=['18', '19', '20', '21'])
    print(zipcode(30324)[0])
