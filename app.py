from flask import Flask, request, jsonify, render_template
from logic import Cloth
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///static/database/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class User(db.Model):

    email = db.Column(db.String, unique=False, nullable=False, primary_key=True)
    zipcode = db.Column(db.Integer, unique=False, nullable=False)

    def __repr__(self):
        
        return (email, zipcode)

@app.route('/weather', methods=['GET'])
def index():

    return render_template('index.html')


@app.route('/weather/forecast', methods=['POST'])
def forecast():

    if request.json:
        results = Cloth(
            Latitude=request.json.get('latitude'),
            Longitude=request.json.get('longitude'),
            times=request.json.get('times'),
            Gender=request.json['gender'],
            Zipcode=request.json.get('zipcode')
            )
        return jsonify(results), 200
    else:
        return "Send json Data", 500

@app.route('/weather/Subscribe', methods=['POST'])
def Subscribe():

    email = request.form['email']
    zipcode = request.form['zipcode']
    payload = User(email=email, zipcode=zipcode)
    db.session.add(payload)
    db.session.commit()

    return render_template('email.html', text = 'Thank your for Subscribing')


@app.route('/weather/Unsubscribe', methods=['GET', 'POST'])
def Unsubscribe():

    if request.method == 'GET':
        email = request.args.get('email')
        if email is None:
            return render_template('unsub.html')
    if request.method == 'POST':
        email = request.form['email']

    User.query.filter_by(email=email).delete()
    db.session.commit()

    return render_template('email.html', text = 'You have been Unsubscribed.')

if __name__ == '__main__':

    app.run(debug=True, host='0.0.0.0')
