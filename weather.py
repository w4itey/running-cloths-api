import requests
import datetime


class Weather:

    def __init__(self):

        self.url = 'https://api.weather.gov'

    def getForecast(
            self,
            latitude=None,
            longitude=None,
            forcasttype='forecastHourly',
            times=['07', '12', '18']
            ):

        results = requests.get(f'{self.url}/points/{latitude},{longitude}')
        results = results.json()
        forecast = requests.get(results['properties'][forcasttype])
        forecast = forecast.json()

        today = datetime.datetime.now().strftime("%Y-%m-%d")

        hours = []

        if times is None:
            times = ['07', '12', '18']
        else:
            times = list(times)

        for t in times:
            hours.append(f'{today}T{t}:00:00-04:00')

        weather = []

        for item in forecast['properties']['periods']:
            if item['startTime'] in hours:
                weather.append(item)
        return weather


if __name__ == '__main__':

    y = Weather()
    print(y.getForecast(latitude=33.820529, longitude=-84.370933))
